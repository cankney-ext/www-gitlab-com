---
layout: markdown_page
title: "Category Direction - Fulfillment Admin Tooling"
description: "The Fulfillment Admin Tooling strategy page belongs to the Fulfillment Platform group of the Fulfillment stage"
---

- TOC
{:toc}

## Overview

The Admin Tooling group exists to provide internal teams with tooling and automation that allow them to provide support to our customers. Our group strives to build tools that are intuitive, easy to use, and optimise team workflows.

When our teams lack the appropriate tools to solve customer problems it leads to massive inefficiencies and, in some scenarios, leads to problems such as customers temporarily losing access to critical paid plan functionality.

Inefficiencies can present in the following ways:

- It is hard to find the customer and account information because: our internal platform lacks functionality and is not intuitive, the information is scattered across multiple systems, and the information is inconsistent across systems which creates low trust in the accuracy of the information for some systems.
- Manual intervention leads to data integrity issues, which perpetuates more manual intervention and leads to errors and unexpected states for customer accounts and subscriptions.
- Customer accounts and subscriptions in unexpected states create complexity that is harder to diagnose and troubleshoot, resulting in more time spent and more people involved to resolve the issue.
- When a bug remains unresolved or a feature is missing, it creates the need for a workaround to serve the customer. Workarounds create new workflows that the team supports and add to the ticket volume.
- Some tasks require a lengthy workflow which includes: verification of requestor and request, finding and validating information, seeking and confirming approvals from the correct people, executing the task, communicating and confirming task completion and verifying changes. 

The Fulfillment Admin Tooling team works with internal teams to build robust systems that enable our internal, customer-facing teams better support our customers. 

Key responsibilities
- Focuses on building admin tools for the support team.
- Create transparency for internal teams into customer subscription, billing and licensing.

## Mission

Provide internal teams at GitLab with the tooling they need to serve customers efficiently and in a way that minimizes repeat issues.

## Vision

Our internal teams have access to relevant customer and account information, they are able to effectively diagnose and troubleshoot issues and errors, and they are able to efficiently complete tasks and resolve issues and errors with the appropriate tooling and automation.

### Target Audience and Experience

Our audience are GitLab internal team members, with a focus on team members that help support our commercial operations. In particular: 
- [GitLab L&R Support team](https://about.gitlab.com/handbook/support/)
- [GitLab Sales team](https://about.gitlab.com/handbook/sales/)
- [GitLab Billing](https://about.gitlab.com/handbook/finance/accounting/finance-ops/billing-ops/) and Accounts Receivable team

We work in collaboration with the Support Operations and Field Operations teams to achieve internal team efficiency goals.

## Challenges to address

### Visibility and data integrity

Our internal teams don't always have the visibility they need to quickly serve a customer. The longer it takes to find information, troubleshoot, and then resolve an issue, the more likely customer dissatisfaction becomes. Our internal systems should provide our teams with access to all the relevant information they need (preferably in one location) to resolve problems or complete a request. 

Where there are long-standing bugs or missing customer-facing features, the Support team has built the Mechanizer tool as a temporary work-around to address required functionality for troubleshooting and resolving customer issues and tasks. Since Mechanizer is not built or maintained by a GitLab engineering team and it leads to data integrity issues, it is important that this tool is deprecated in favour of intuitive tooling and automation that can provide needed functionality.

**Projects to tackle this challenge:**

- [Migrate Mechanizer features so it can be deprecated](https://gitlab.com/groups/gitlab-org/-/epics/6828)
- [Build controls for namespaces on a trial in CustomersDot](https://gitlab.com/groups/gitlab-org/-/epics/9451)
- [Build controls for namespaces with a subscription in CustomersDot](https://gitlab.com/groups/gitlab-org/-/epics/9453)
- [Build tools for license generation that enable the Sales team to unblock customer during renewals process](https://gitlab.com/groups/gitlab-org/-/epics/9732)
- [Implement minutes and storage change ability in CustomersDot](https://gitlab.com/groups/gitlab-org/-/epics/9031)
- [CustomersDot Admin maintenance](https://gitlab.com/groups/gitlab-org/-/epics/9488)
- [CustomersDot Admin usability](https://gitlab.com/groups/gitlab-org/-/epics/9454)
- [Auditing and activity logging](https://gitlab.com/groups/gitlab-org/-/epics/9348)

### Efficiency

What does an efficient L&R Support team look like?

- Easy to find the information needed to troubleshoot a problem.
- Minimal involvement required from other teams to troubleshoot a problem or complete a task.
- Able to access information quickly and ideally from a single source.

What does an efficient Sales team look like?

- Able to easily and quickly provide customer with access to GitLab product as needed to support the sales process.
- Approvals processes are intuitive and do not hamper the sales process.
- Has visibility into relevant customer information to support sales process or to proactively or reactively resolve customer issues.

**How we will tackle this:**

- Review inefficient workflows and identify opportunities for tooling and automation to cut down on time spent searching for relevant information and completing tasks.
- Identify workflows where multiple teams are involved to complete a task and explore solutions to bring ownership and supportive tooling to the relevant team.
- Review inefficiencies in approvals processes and explore opportunities for tooling or automation to improve efficiency.

## KPIs

- Volume of manual subscription modifications by support.
- Volume of internal requests from Sales to L&R Support: [Chart showing month over month volume](https://app.periscopedata.com/app/gitlab/1106588/Fulfillment:-Admin-Tooling-PIs?widget=16021712&udv=0).
- Volume of escalations measured by L&R Support Ticket Attention Requests (STAR): [Chart showing month over month volume](https://app.periscopedata.com/app/gitlab/1106588/Fulfillment:-Admin-Tooling-PIs?widget=16073107&udv=0).
- Ratio of L&R ticket volume vs GitLab subscriptions: [Chart showing ratio month over month for the past 3 years](https://app.periscopedata.com/app/gitlab:safe-dashboard/919356/Supersonics-Executive-Dashboard?widget=14190696&udv=1567705).

## 1 Year Plan

Over the next 12 months, the Admin Tooling team has the following primary objectives:

1. Deprecate the use of Mechanizer and build the needed functionality into our platform (customers.gitlab.com), this initiative is tracked in the following epics:
     - [Namespace controls for trials in customersdot](https://gitlab.com/groups/gitlab-org/-/epics/9451)
     - [Namespace controls for subscriptions in customersdot](https://gitlab.com/groups/gitlab-org/-/epics/9453)
1. Review use cases for Mechanizer functionality and seek to eliminate the need for the functionality that mechanizer was providing by solving for the underlying problems via: self-serve features in the product, enabling internal teams with features specific to their workflows (example: temporary license generation for the sales org), and automation. Efforts towards this goal are tracked in each review epic:
     - [Review `Update GitLab Plan` mechanizer function](https://gitlab.com/groups/gitlab-org/-/epics/8423)
     - [Review `Force reassociation` mechanizer function](https://gitlab.com/groups/gitlab-org/-/epics/8425)
     - [Review `Clear subscription` mechanizer function](https://gitlab.com/groups/gitlab-org/-/epics/8424)
1. Implement audit trail for actions performed where customer accounts/namespaces/subscriptions are modified for the purpose of supporting compliance and measuring volume of modifications.
1. Create, update and maintain admin tooling functionality on our platform and ensure that features keep data integrity, with clear errors and activity logging.

For a list of upcoming and ongoing projects, see our [GitLab epic roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&label_name%5B%5D=group%3A%3Aadmin+tooling&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP). The roadmap view is colour-coded to show: blue for feature work, green for ongoing/evergreen initiatives, and orange for sales-specific feature work.

## Prioritisation and data insights

Our [performance indicator dashboard](https://app.periscopedata.com/app/gitlab/1106588/Fulfillment:-Admin-Tooling-PIs) gives us visibility into data relating to tickets.

From the dashboard we can [see that internal requests are increasing](https://app.periscopedata.com/app/gitlab/1106588/Fulfillment:-Admin-Tooling-PIs?widget=16021712&udv=0) month over month. The [types of internal requests](https://about.gitlab.com/handbook/support/internal-support/#internal-requests) include extending a trial, modifying a subscription, creating or troubleshooting a license issue, and investigating seat count or provisioning discrepancies. Internal requests often require the use of Mechanizer when admin actions fail with our current internal tools, known bugs hamper the ability to self-serve or there is a gap in our feature offering that is filled with manual Support assistance. 

Mechanizer is a tool that uses rails console functions to create and modify SaaS subscriptions, orders, and namespaces.

#### Why are we prioritising the deprecation of mechanizer? 

Since we know that internal requests and the use of Mechanizer is linked, our long-term goal, together with other Fulfillment and internal teams, is to decrease the total volume of internal requests. In the shorter term, we recognise that Mechanizer is a burden to the Support team, who have created and maintain a tool that should not have existed and that continues to be a sunken cost.

The cost of mechanizer, since the first commit in May 2020, is summarized below:

- The time and attention of about 26 GitLab team members (across Support, SupportOps and Fulfillment) that have contributed to the code, documentation and enablement for Mechanizer, console troubleshooting and training, zendesk integration, and form creation.
- The ongoing maintenance of code, bug fixes and minor improvements by 2-3 individuals. In June 2022 it was agreed that major improvements or new functionality will no longer be built.
- About 1230 lines of code currently for console script and mechanizer.
- Several handbook pages and documentation written for workflows, troubleshooting, and general usage guidelines around mechanizer.
- Every time a mechanizer function is used, a pipeline runs. Each pipeline averages between 0:40 - 1:00 and there have been around 2000 pipelines already run; 0:50x2000 equates to about 1667 compute credits.

Please see [this thread](https://gitlab.com/groups/gitlab-org/-/epics/6828#note_1230510840) for a more detailed description. 
